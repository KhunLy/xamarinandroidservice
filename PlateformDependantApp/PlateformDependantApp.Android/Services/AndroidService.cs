﻿using PlateformDependantApp.DependencyInjection;
using Xamarin.Forms;

[assembly: Dependency(typeof(PlateformDependantApp.Droid.Services.AndroidService))]
namespace PlateformDependantApp.Droid.Services
{

    public class AndroidService : IService
    {
        public string DoSomething()
        {
            return "Hello World !";
        }
    }
}