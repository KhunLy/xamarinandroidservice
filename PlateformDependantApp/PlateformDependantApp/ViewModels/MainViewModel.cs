﻿using PlateformDependantApp.DependencyInjection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace PlateformDependantApp.ViewModels
{
    class MainViewModel : BindableObject
    {

        private string _toDisplay;

        public string ToDisplay
        {
            get { return _toDisplay; }
            set { _toDisplay = value; OnPropertyChanged(); }
        }

        public MainViewModel()
        {
            ToDisplay = DependencyService.Get<IService>().DoSomething();
        }

    }
}
