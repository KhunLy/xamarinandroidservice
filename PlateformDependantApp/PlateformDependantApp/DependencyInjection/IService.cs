﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlateformDependantApp.DependencyInjection
{
    public interface IService
    {
        string DoSomething();
    }
}
